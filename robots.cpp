#include "stdafx.h"
#include <windows.h>
#include <glut.h>
//#include "bots.h"
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
using namespace std;

#define PLAYER_RAD 0.05
#define TURREL_HEIGHT 0.01
#define TURREL_WEIGHT 0.04
#define MAX_STEP 0.005
#define PI 3.1415926
#define BULLET_SPEED 0.1
#define BULLET_ACCELERATION -0.005
#define ROTATION_STEP PI/8
#define ROT_STEP PI/36

enum {MOVE, SHOOT, TURN};
enum {NO_GAME, GAME, PAUSE, WINNER, DEAD_HEAT, GAME_OVER};

int isGame = NO_GAME;
//-----------------------������-----------------------
class Field;
class Bullet;
class Bot;
//-----------------------������-----------------------
//����������
class Coordinate{
public:
	Coordinate(){x = 0; y = 0;};
	Coordinate(int _x, int _y){x = _x; y = _y;};
	Coordinate& operator=(const Coordinate& a){x=a.x; y=a.y;return *this;};
	Coordinate(const Coordinate& a){x=a.x, y=a.y;};
	double x;
	double y;
};

double makeAngle(double angle){if (angle>3*PI/2) return angle - 2*PI; if (angle<-PI/2) return angle+2*PI; return angle; }
//����
class Bullet{
    Coordinate center;
    double angle;
    double v;
    double a;
	int time;
	int id;
public:
    Bullet(Coordinate _center, double _angle, int _id): center(_center),angle(_angle){v=BULLET_SPEED;  a=BULLET_ACCELERATION; time =0 ; id = _id; };
    int fly();
    Coordinate getLocation(){ return center;};
    double getDirection(){return angle;};
};
//�����
class Bot{
public:
	Bot(){ _center=Coordinate(0, 0); id = 0; canShoot = 20;};
    Bot(Coordinate center, double rotation, int robot_id ) { _center = center; id = robot_id; canShoot = 5;}
    virtual void makeStep() = 0;
    virtual void nextStep() = 0;
	Coordinate getLocation(){return _center;};
	double getDirection(){return _rotation;};
	int getID(){return id;};
protected:
	virtual void shoot() = 0;
    virtual void move(double len) = 0;
    double _rotation;
    Coordinate _center;
    int actionType;
	int id;
	int canShoot;
	double info;
    
};

class SimpleBot : public Bot{
protected:
    SimpleBot(){_center.x = 0; _center.y =0; _rotation = 0; };
    SimpleBot(Coordinate center, double rotation){ _center = center;_rotation = rotation; };
    virtual void makeStep();
    virtual void nextStep() = 0;
protected:
	void shoot();
    void move(double len);
};

void SimpleBot::makeStep(){
	switch(actionType){
	case SHOOT:
		shoot(); break;
	case MOVE:
		move(info); break;
	case TURN:
		_rotation = makeAngle(info);
		break;
	}
};

bool bulletIsVisible(Coordinate center, double angle, Coordinate bullet){
	double difX = bullet.x - center.x;
	double difY = bullet.y - center.y;
	double angle1 = atan( difY/difX );
	if (difX<0) angle1+=2*PI;
	if ((angle1<0)&&(angle>3*PI/2)) angle1 +=2*PI;
	else if ((angle<0)&&(angle1>3*PI/2)) angle+=2*PI;
	if ((angle1<(angle+ROTATION_STEP))&&(angle1>(angle-ROTATION_STEP))) return true;
	return false;
}
bool botIsVisible(Coordinate center, double angle, Coordinate bot){
	double difX = bot.x - center.x;
	double difY = bot.y - center.y;
	double length = difX*difX + difY*difY;
	double angle1 = atan( difY/difX );
	if (difX<0) angle1+=2*PI;
	double angle2 = asin(PLAYER_RAD/length);
	if ((angle1<0)&&(angle>3*PI/2)) angle1 +=2*PI;
	else if ((angle<0)&&(angle1>3*PI/2)) angle+=2*PI;
	if (((angle1-angle2)<(angle+ROTATION_STEP))&&((angle1-angle2)>(angle-ROTATION_STEP))) return true;
	else if (((angle1+angle2)<(angle+ROTATION_STEP))&&((angle1+angle2)>(angle-ROTATION_STEP))) return true;
	else return false;
}

class BerserkBot : public SimpleBot{
public:
	BerserkBot(Coordinate center, double rotation, int _id){ _center = center; _rotation = rotation; id = _id; canShoot=20;};
	virtual void nextStep();
};


class BeagleBot : public SimpleBot{
public:
	BeagleBot(Coordinate center, double rotation, int _id){ _center = center;  id = _id; canShoot=20; targetID=-1;};
	virtual void nextStep();
private:
	int targetID;
};

class AssasinBot : public SimpleBot{
public:
	AssasinBot(Coordinate center, double rotation, int _id){ _center = center; _rotation = rotation;  id = _id; canShoot = 20;};
	virtual void nextStep();
};

class Field {
public:
    static Field& Instance(){
    static Field _instance;
    return _instance;
	};
	vector<Bullet> getBullets(){return bullets;};
	vector<Bot*> getBots(){return bots;};
	void appendBullet(Bullet bullet){bullets.push_back(bullet);};
    void newGame();
    void update();

private:
    Field() {isGame = NO_GAME;};
    Field(const Field&){};
    Field& operator=(const Field&){};
	void GameOver();
    vector<Bot *> bots;
    vector<Bullet> bullets;
};

void Field::update(){
	int _isGame  = isGame;
	int rSize, bSize, size, bKey;
	vector<int> lostBullets, lostBots;
	switch(_isGame)
	{
	case GAME:
	rSize = bots.size(); bSize = bullets.size(); 
	for (int i=0; i<rSize; i++) 
		bots[i]->nextStep();
	for (int i=0; i<rSize; i++) bots[i]->makeStep();
	for (int i=0; i<bSize; i++) {
		bKey = bullets[i].fly();
		switch(bKey){
		//��� ���������. ���� ����������� �� ���������
		case -2: break;
		//���� ����� ��� �������� �� ������� ����
		case -1: 
			lostBullets.push_back(i); break;
		default:
		//� ����-�� ������
			for (int j=0; j<lostBots.size(); j++) 
				if (lostBots[j]==bKey) 
					break;//����� �� �������� ��� ������
			
			lostBots.push_back(bKey);
			lostBullets.push_back(i);
		}
	}
	bSize = lostBullets.size();
	size = lostBots.size();
	if (size)
	for (int i=size-1; i>=0; i--) 
	{
		bots.erase(bots.begin()+lostBots[i]);
	}
	if (bSize)
	for (int i=bSize-1; i>=0; i--) 
		bullets.erase(bullets.begin()+lostBullets[i]);
	lostBullets.clear(); lostBots.clear(); 
	if (bots.size()==1) isGame = WINNER;
	if (!bots.size()) isGame = DEAD_HEAT;
	break;
	case WINNER:
		if (bots.size())
			cout<<"Winnner is "<<bots[0]->getID()<<endl;
		GameOver();
		break;
	case DEAD_HEAT:
		cout<<"Dead heat\n";
		GameOver();
		break;
	case GAME_OVER:
		GameOver();
		break;
	}
} 

void Field::newGame(){
	isGame = GAME;
	bots.clear();
	bullets.clear();
    double angle = 120;
    Coordinate coord;
    for (int i=0; i<3; i++){
        angle*=i;
        coord.x=0.1+0.3*i;
        coord.y=0.3*i;
		bots.push_back(new BerserkBot(coord, angle, i));
    }
	for (int i=3; i<6; i++){
        angle*=i;
        coord.x=-0.1 - 0.3*(i%3);
        coord.y=0.3*(i%3);
		bots.push_back(new BeagleBot(coord, angle, i));
    }
	for (int i=6; i<9; i++){
        angle*=i;
        coord.x=0;
        coord.y=-0.1-0.3*(i%3);
		bots.push_back(new AssasinBot(coord, angle, i));
    }
	
}

double distanc(Coordinate center, double angle, Coordinate target){
	double A, B, C, D, R, X1, X2, Y1, Y2, Len1, Len2;
	bool half = false;//� ����� �������� ��������� false - 1, 4 ��������, true - 2,3
	if ((angle>PI/2)&&(angle<3*PI/2)) half = true;
	//y = Ax + B - ���������� ������ ����
	A = tan(angle);
	B = center.y - A*center.x ;
	//(x-C)^2+(y-D)^2 = R^2 - ������� ������
	C = target.x;
	D = target.y;
	R = PLAYER_RAD;
	//���� ����� �����������
	double discr = (A*(B-D)-C)*(A*(B-D) - C)-(C*C + (B-D)*(B-D) - R*R)*(A*A+1);
	//� �� ����������
	if (discr<0) return -1;
	double firstPart = (A*D+C-A*B);
	X1 = (firstPart + sqrt(discr));
	X1/=(A*A+1);
	X2 = (firstPart*10000 - sqrt(discr)*10000)/10000;
	X2/= (A*A+1);
	Y1 = A*X1 + B;
	Y2 = A*X2 + B;
	//ta(a) = tg(a+�/2), ������� ���������
	bool check1 = false;
	if (X1<center.x) check1 = true;
	bool check2 = false;
	if (X2<center.x) check2 = true;
	if ((check1!=half)||(check2!=half)) 
		return -1;
	Len1 = sqrt((X1-center.x)*(X1 - center.x)+(Y1-center.y)*(Y1 - center.y));
	Len2 = sqrt((X2-center.x)*(X2 - center.x)+(Y2-center.y)*(Y2 - center.y));
	if (Len1>Len2) return Len2; else return Len1;
}

int Bullet::fly(){
        double max_lenght = v;
        if (0>=max_lenght) return -1; //���� �����
		Field &f = Field::Instance();
		vector<Bot *> bots = f.getBots();
		int size = bots.size();
		double lenght, angle1, minLen = max_lenght;
		int rid =-1;//���  ������
		for (int i=0; i<size; i++){
			Coordinate coord = bots[i]->getLocation();
			lenght = distanc(center, angle, coord);
			if ((lenght!=-1)&&(lenght<minLen)&&(lenght>0)) 
			{rid = i; minLen = lenght;}
		}
		time++;
		
		if (rid!=-1){
			int r = rand()%20;
			//cout<<"rand "<<r<<"time "<<time<<endl;
			if (r> time) return rid;
			else return -1;
		}
        center.x+=max_lenght*cos(angle);
        center.y+=max_lenght*sin(angle);
		//���� �������
		if ((center.x<-1)||(center.x>1)||(center.y<-1)||(center.y>1)) return -1;
        v+=a; //����������� ������
		//�� ��� ��� �����
        return -2;
}

void SimpleBot::shoot(){
	Field &f = Field::Instance();
	f.appendBullet(Bullet(Coordinate(_center), _rotation, id));
	//cout<<"shoot "<<id<<endl;
}

void BerserkBot::nextStep(){
	Field &f = Field::Instance();
	//���� �� ����� � ���� ���������?
	vector<Bot *> bots = f.getBots();
	int size = bots.size();
	double lenght, angle1, angle2, minLen = MAX_STEP;
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
		Coordinate coord = bots[i]->getLocation();
		if (distanc(_center, _rotation, coord)!=-1)
		if (!canShoot){
			actionType=SHOOT;
			canShoot=20;
			return;
		}else {
			actionType=MOVE;
			info = MAX_STEP;
			canShoot--;
			return;
		}	}
	if (canShoot) canShoot--;
	//���� ����� � ���� ���������?
	size = bots.size();
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
	if (botIsVisible(_center, _rotation, bots[i]->getLocation()))
		{ 
			double difX = bots[i]->getLocation().x - _center.x;
			double difY = bots[i]->getLocation().y - _center.y;
			double length = difX*difX + difY*difY;
			double angle1 = atan( difY/difX );
			if (difX<0) angle1+=2*PI;
			actionType = TURN; if (angle1>(_rotation+ROT_STEP)) {info = _rotation + ROT_STEP; return;}
			if (angle1<(_rotation-ROT_STEP)) {info = _rotation - ROT_STEP; return;}
			info = angle1; return; }
	}
	//� ��� ����� ����?
	vector <Bullet> bullets = f.getBullets();
	size = bullets.size();
	int isBullet = -1;
	for (int i=0; i<size; i++) {
		if (bulletIsVisible(_center, _rotation, bullets[i].getLocation()))
		if (distanc(bullets[i].getLocation(), bullets[i].getDirection(), _center)!=-1) { isBullet=i; break;}
	}
	//� ��� ����� ����
	if (isBullet!=-1){
		//���������� �������� ��������� � ���������� ������ ����, ������������
		double dir = bullets[isBullet].getDirection();
		if ((dir==_rotation)||(dir==_rotation-PI)||(dir==_rotation+PI)) {actionType = TURN; info=_rotation+ROT_STEP;return;}
		//���������� �������� ��������� � ���������� ������ ����, ���� ������
		else
			//C���� � ������
			if (((_center.x==(-1+PLAYER_RAD))||(_center.y==(-1+PLAYER_RAD))||(_center.x==(1-PLAYER_RAD))||(_center.y==(1-PLAYER_RAD))))
				if (actionType!=TURN) { actionType = TURN; info = _rotation+ROT_STEP; return;} //���� ���������
				else {actionType = MOVE; info = -MAX_STEP; return;} //����� �����
			else{actionType = MOVE; info = MAX_STEP; return;}
	}
	//������ �� �����
	actionType = TURN; info = _rotation+ROT_STEP; return;
	
}

void BeagleBot::nextStep(){
	bool isTarget = false;
	Bot *target;
	Field &f = Field::Instance();
	vector<Bot *> bots = f.getBots();
	int size = bots.size();
	for (int i =0; i<size; i++)
		if (bots[i]->getID()==targetID) {isTarget = true; break;}
	if (isTarget)
	{
		target = bots[targetID];
		double dis = distanc(_center, _rotation, target->getLocation());
		if (dis!=-1)
			if ((dis<1)&&(!canShoot)) 
			{actionType = SHOOT; canShoot = 20; return; }
			else {actionType = MOVE; info = dis; if (canShoot) canShoot --; return;}
		else if (botIsVisible(_center, _rotation, target->getLocation())) { 
			if (canShoot) canShoot --;
			double difX = target->getLocation().x - _center.x;
			double difY = target->getLocation().y - _center.y;
			double length = difX*difX + difY*difY;
			double angle1 = atan( difY/difX );
			if (difX<0) angle1+=2*PI;
			actionType = TURN; if (angle1>(_rotation+ROT_STEP)) {info = _rotation + ROT_STEP; return;}
			if (angle1<(_rotation-ROT_STEP)) {info = _rotation - ROT_STEP; return;}
			info = angle1; return;
		}
		target = NULL;
	}
	double lenght, angle1, angle2, minLen = MAX_STEP;
	//���� �� ����� � ���� ���������?
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
		Coordinate coord = bots[i]->getLocation();
		double dis = distanc(_center, _rotation, coord);
		if (dis!=-1)
		{
			target = bots[i]; targetID = i;
			if ((dis<1)&&(!canShoot)) {actionType = SHOOT; canShoot = 20; return; }
			else {actionType = MOVE; info = dis;  return;}	
		} 
	}
	if (canShoot) canShoot--;
	//���� ����� � ���� ���������?
	size = bots.size();
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
	if (botIsVisible(_center, _rotation, bots[i]->getLocation()))
		{ 
			targetID = i;
			double difX = bots[i]->getLocation().x - _center.x;
			double difY = bots[i]->getLocation().y - _center.y;
			double length = difX*difX + difY*difY;
			double angle1 = atan( difY/difX );
			if (difX<0) angle1+=2*PI;
			actionType = TURN; if (angle1>(_rotation+ROT_STEP)) {info = _rotation + ROT_STEP; return;}
			if (angle1<(_rotation-ROT_STEP)) {info = _rotation - ROT_STEP; return;}
			info = angle1; return; }
	}
	targetID = -1;
	//� ��� ����� ����?
	vector <Bullet> bullets = f.getBullets();
	size = bullets.size();
	int isBullet = -1;
	for (int i=0; i<size; i++) {
		if (bulletIsVisible(_center, _rotation, bullets[i].getLocation()))
		if (distanc(bullets[i].getLocation(), bullets[i].getDirection(), _center)!=-1) { isBullet=i; break;}
	}
	//� ��� ����� ����
	if (isBullet!=-1){
		//���������� �������� ��������� � ���������� ������ ����, ������������
		double dir = bullets[isBullet].getDirection();
		if ((dir==_rotation)||(dir==_rotation-PI)||(dir==_rotation+PI)) {actionType = TURN; info=_rotation+ROT_STEP;return;}
		//���������� �������� ��������� � ���������� ������ ����, ���� ������
		else
			//C���� � ������
			if (((_center.x==(-1+PLAYER_RAD))||(_center.y==(-1+PLAYER_RAD))||(_center.x==(1-PLAYER_RAD))||(_center.y==(1-PLAYER_RAD))))
				if (actionType!=TURN) { actionType = TURN; info = _rotation+ROT_STEP; return;} //���� ���������
				else {actionType = MOVE; info = -MAX_STEP; return;} //����� �����
			else{actionType = MOVE; info = MAX_STEP; return;}
	}
	//������ �� �����
	actionType = TURN; info = _rotation+ROT_STEP; return;
}


void AssasinBot::nextStep(){
	Field &f = Field::Instance();
	vector<Bullet> bullets = f.getBullets();
	int size = bullets.size();
	int isBullet = -1;
	for (int i = 0; i<size; i++){
		if (bulletIsVisible(_center, _rotation, bullets[i].getLocation()))
		if (distanc(bullets[i].getLocation(), bullets[i].getDirection(), _center)!=-1) { isBullet=i; break;}
	}
	if (isBullet!=-1) {
		//���������� �������� ��������� � ���������� ������ ����, ������������
		double dir = bullets[isBullet].getDirection();
		if ((dir==_rotation)||(dir==_rotation-PI)||(dir==_rotation+PI)) {actionType = TURN; info=_rotation+ROT_STEP;return;}
		//���������� �������� ��������� � ���������� ������ ����, ���� ������
		else
			//C���� � ������
			if (((_center.x==(-1+PLAYER_RAD))||(_center.y==(-1+PLAYER_RAD))||(_center.x==(1-PLAYER_RAD))||(_center.y==(1-PLAYER_RAD))))
				if (actionType!=TURN) { actionType = TURN; info = _rotation+ROT_STEP; return;} //���� ���������
				else {actionType = MOVE; info = -MAX_STEP; return;} //����� �����
			else{actionType = MOVE; info = MAX_STEP; return;}
	}
	vector <Bot *> bots = f.getBots();
	size = bots.size();
	for (int i=0; i<size; i++)
		if (botIsVisible(bots[i]->getLocation(), bots[i]->getDirection(), _center)){
			//���������� �������� ��������� � ����������� �������� ����������
			double dir = bots[i]->getDirection();
			if ((dir==_rotation)||(dir==_rotation-PI)||(dir==_rotation+PI)) {actionType = TURN; info=_rotation+ROT_STEP;return;}
			//���������� �������� �� ��������� � ����������� �������� ����������, ���� ������
			else
			//C���� � ������
			if (((_center.x==(-1+PLAYER_RAD))||(_center.y==(-1+PLAYER_RAD))||(_center.x==(1-PLAYER_RAD))||(_center.y==(1-PLAYER_RAD))))
				if (actionType!=TURN) { actionType = TURN; info = _rotation+ROT_STEP; return;} //���� ���������
				else {actionType = MOVE; info = -MAX_STEP; return;} //����� �����
			else{actionType = MOVE; info = MAX_STEP; return;}
		}
	double lenght, angle1, angle2, minLen = MAX_STEP;
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
		Coordinate coord = bots[i]->getLocation();
		if (distanc(_center, _rotation, coord)!=-1)
		if (!canShoot){
			actionType=SHOOT;
			canShoot=20;
			return;
		}else {
			actionType=MOVE;
			info = MAX_STEP;
			canShoot--;
			return;
		}	}
	if (canShoot) canShoot--;
	//���� ����� � ���� ���������?
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
	if (botIsVisible(_center, _rotation, bots[i]->getLocation()))
		{ 
			double difX = bots[i]->getLocation().x - _center.x;
			double difY = bots[i]->getLocation().y - _center.y;
			double length = difX*difX + difY*difY;
			double angle1 = atan( difY/difX );
			if (difX<0) angle1+=2*PI;
			actionType = TURN; if (angle1>(_rotation+ROT_STEP)) {info = _rotation + ROT_STEP; return;}
			if (angle1<(_rotation-ROT_STEP)) {info = _rotation - ROT_STEP; return;}
			info = angle1; return; }
	}
	actionType = TURN; info = _rotation + ROT_STEP; 
}

void SimpleBot::move(double len){
    int dir = 1;
	bool isTooClose = false;
	if (len<0) dir = -1;
    if (len>MAX_STEP) len = MAX_STEP;
	Field &f=Field::Instance();
	vector<Bot *> bots = f.getBots();
	Coordinate coord, bot;
	coord.x = _center.x+dir*len*cos(_rotation);
	coord.y = _center.y+dir*len*sin(_rotation);
	int size = bots.size();
	for (int i=0; i<size; i++) if (bots[i]->getID()!=id){
		bot = bots[i]->getLocation();
		double dis = distanc(coord, _rotation, bot);
		if (dis!=-1)
		{double dif = dis- 2*PLAYER_RAD;
		if (dif<=0) {
			len+=dif; isTooClose = true;} 
		}}
	if (isTooClose){
		coord.x = _center.x+dir*len*cos(_rotation);
		coord.y = _center.y+dir*len*sin(_rotation);
	} 
	if (coord.x<-1+PLAYER_RAD) coord.x = PLAYER_RAD;
	else if (coord.x>1-PLAYER_RAD) coord.x=1-PLAYER_RAD;
	if (coord.y<-1+PLAYER_RAD) coord.y = PLAYER_RAD;
	else if (coord.y>1-PLAYER_RAD) coord.y=1-PLAYER_RAD;
	_center = coord;
}

void Field::GameOver(){
	if (bots.size())
		for (int i=0; i<bots.size(); i++){
			delete bots[i];
			bots[i]=NULL;
		}
	exit(0);
}

//----------------------------���������-------------------------------------------
void Initialize() {
//������� ������� (���������) ����
glClearColor(1.0,0.0,1.0,1.0);
 
//���������� ��������
glMatrixMode(GL_PROJECTION);
glLoadIdentity();
glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0);

//������������� ��������� ������
Field& field = Field::Instance();
field.newGame();
srand((unsigned)time(NULL));
}

void drawPolygon(double x,double y,double height,double weight, double rotation){
//��������� �������� 
double x1, y1, x2 , y2;
const double angle = atan(height/weight);
const double diag = sqrt(weight*weight+height*height);
//cout << "angle "<<angle/3.14*180<<endl;
x1 = cos(angle + rotation)*diag;
//cout<< "x1 "<<x1<<endl;
y1 = sin(angle + rotation)*diag;
//cout<< "y1 " << y1<< endl;
x2 = cos(PI - angle + rotation)*diag;
//cout<<"x2 "<<x2<<endl;
y2 = sin(PI - angle + rotation)*diag;
//cout<<"y2 "<<y2<<endl;
//glColor3f(0.3,0.8,0.1); //������� ����
glBegin(GL_POLYGON);
glVertex3f(x1+x, y1+y, 0.0); //���������� ��������
glVertex3f(x-x2, y-y2, 0.0);
glVertex3f(x-x1, y-y1, 0.0);
glVertex3f(x2+x, y2+y, 0.0);
glEnd();
}

void drawTurrel(double x,double y, double rotation, int colourId){
    x += TURREL_WEIGHT*cos(rotation);
    y += TURREL_WEIGHT*sin(rotation);
    //std::cout<<"y"<<y<<"\n";
    glColor3f(0.6*(!(colourId/3)),0.6*((colourId/3)==1),0.6*((colourId/3)==2)); //������
    drawPolygon(x, y, PLAYER_RAD/2, PLAYER_RAD/2, rotation);
    drawPolygon(x, y, TURREL_HEIGHT, TURREL_WEIGHT, rotation);
}

void drawBody(double X,double Y, double radius, int colourId){ // ������������� ���� ����������.
            glColor3f(0.8*(!(colourId/3)),0.8*((colourId/3)==1),0.8*((colourId/3)==2)); //�������
            // ���������� ���������� ���������������.
            glBegin(GL_TRIANGLE_FAN);
                // ������������� ����� ����������.
                glVertex2d(X, Y);
                for (int angle = 0; angle <= 360; angle += 10)
                {
                    // ���������� x, y ��������� �� �������� ���� ������������ ������ ���������.
                    double x = radius * cos(angle * PI / 180);
                    double y = radius * sin(angle * PI / 180);
                    // ������� ���������� � � ������ [xCentre; yCentre].
                    glVertex2d(x + X, y + Y);
                }
            glEnd();
}


void Draw()
{
//������� ����� 
	glClear(GL_COLOR_BUFFER_BIT);
	glPushMatrix();   
    Field& field = Field::Instance();
	vector<Bot *> bots = field.getBots();
	int size = bots.size();
   
	for (int i=0; i<size; i++){
		drawBody(bots[i]->getLocation().x, bots[i]->getLocation().y,  PLAYER_RAD, bots[i]->getID());
		drawTurrel(bots[i]->getLocation().x, bots[i]->getLocation().y, bots[i]->getDirection(), bots[i]->getID());
	}
	vector<Bullet> bullets = field.getBullets();
	size = bullets.size();
	glColor3f(1,1,1); 
	for (int i=0; i<size; i++)
		drawPolygon(bullets[i].getLocation().x, bullets[i].getLocation().y, 0.005, 0.005, 0);	

    glFlush();
	glPopMatrix();
	glutSwapBuffers();
	Sleep(50);
}
 
void KeyDown(unsigned char key, int x, int y){
	switch (key)
	{
		case 27:
		case 'q':
		case 'Q': isGame = GAME_OVER; break;
		case ' ':
			if (isGame==GAME) {isGame = PAUSE; break;}
			if (isGame==PAUSE) {isGame = GAME; break;}
	}
}
void timer(int = 0){
	Field &f=Field::Instance();
	f.update();
	Draw();
	glutTimerFunc(10, timer, 0);
	//while(1);
}

//����� � ������� ����
int main(int argc, char **argv)
{
	Initialize();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(600, 600);
	glutInitWindowPosition(20, 20);
	glutCreateWindow("Battle_emu");
	glClearColor(0, 0, 0, 1.0);
	glutKeyboardFunc(KeyDown);
	glutDisplayFunc(Draw);
	timer();
	glutMainLoop(); 
    return 0;
}